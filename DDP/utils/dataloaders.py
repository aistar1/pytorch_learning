
import numpy as np
import random
import os

import torch
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, distributed

PIN_MEMORY = str(os.getenv('PIN_MEMORY', True)).lower() == 'true'  # global pin_memory for dataloaders

def seed_worker(worker_id):
    # Set dataloader worker seed https://pytorch.org/docs/stable/notes/randomness.html#dataloader
    worker_seed = torch.initial_seed() % 2 ** 32
    np.random.seed(worker_seed)
    random.seed(worker_seed)
    # print(f'worker_id: {worker_id}    {torch.utils.data.get_worker_info()} \n')


def create_dataloader(download_path='./', is_training=True, batch_size=64, workers=4):
    if is_training:
        train_transform = transforms.Compose([transforms.RandomResizedCrop(224),
                                            transforms.RandomHorizontalFlip(),
                                            transforms.ToTensor(),
                                            transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                                                                std=[0.229, 0.224, 0.225])])
    else:
        train_transform = transforms.Compose([transforms.Resize(256),
                                            transforms.CenterCrop(224),
                                            transforms.ToTensor(),
                                            transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                                                                std=[0.229, 0.224, 0.225])])

    train_dataset = torchvision.datasets.CIFAR10(root='./data', train=is_training, download=True, transform=train_transform)

    train_sampler = distributed.DistributedSampler(train_dataset)
    
    nd = torch.cuda.device_count()  # number of CUDA devices
    nw = min([os.cpu_count() // max(nd, 1), workers])  # number of workers
    g = torch.Generator()
    g.manual_seed(0)

    train_loader = DataLoader(train_dataset,
                            sampler=train_sampler,
                            batch_size=batch_size,
                            pin_memory=PIN_MEMORY,
                            num_workers=nw,
                            worker_init_fn=seed_worker,
                            generator=g)

    return train_loader