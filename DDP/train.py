
import os
import argparse
import time
from utils.dataloaders import create_dataloader
from models.model import resnet18
from utils.general import init_seeds, process_status
from utils.torch_utils import torch_distributed_zero_first

import torch
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.optim import lr_scheduler
import torch.nn as nn

LOCAL_RANK = int(os.getenv('LOCAL_RANK', -1))  # https://pytorch.org/docs/stable/elastic/run.html
RANK = int(os.getenv('RANK', -1))
WORLD_SIZE = int(os.getenv('WORLD_SIZE', 1))

def parse_opt():
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int, default=100, help='total training epochs')
    parser.add_argument('--batch-size', type=int, default=16, help='total batch size for all GPUs')
    parser.add_argument('--seed', type=int, default=0, help='Global training seed')
    parser.add_argument('--workers', type=int, default=4, help='max dataloader workers (per RANK in DDP mode)')
    return parser.parse_known_args()[0]

def main(opt):
    torch.cuda.set_device(LOCAL_RANK)
    if torch.cuda.is_available():
        device = torch.device("cuda", LOCAL_RANK)
    else:
        device = torch.device("cpu")

    dist.init_process_group(backend='nccl' if dist.is_nccl_available() else 'gloo', init_method='env://')

    #dist.barrier()
    # rank = dist.get_rank()
    # world_size = dist.get_world_size()
    
    print(process_status())
    init_seeds(opt.seed + 1 + RANK)
    with torch_distributed_zero_first(LOCAL_RANK):
        # TODO
        None 

    # load data
    train_loader = create_dataloader(
       download_path = './data', is_training = True, batch_size=opt.batch_size, workers=opt.workers)
    if RANK in {-1, 0}:
        valid_loader = create_dataloader(
            download_path = './data', is_training = True, batch_size=opt.batch_size)
    
    # load model
    model = resnet18(nc=10)
    model = model.to(device)

    # SyncBatchNorm
    model = torch.nn.SyncBatchNorm.convert_sync_batchnorm(model).to(device)
    
    # DDP mode
    model = DDP(model, device_ids=[LOCAL_RANK], output_device=LOCAL_RANK)

    # Optimizer
    optimizer = torch.optim.SGD(model.parameters(), lr=0.001, momentum=0.9)

    scheduler = lr_scheduler.StepLR(optimizer, step_size = 30, gamma = 0.1)
    criterion = nn.CrossEntropyLoss().to(device)
    # Start training
    t0 = time.time()
    start_epoch = 0
    for epoch in range(opt.epochs):
        train_loader.sampler.set_epoch(epoch)

        # training
        model.train()
        tloss = 0.0
        accuracy = 0.0
        total_num = 0
        for i, (data, target) in enumerate(train_loader):
            data = data.to(device, non_blocking=True)
            target = target.to(device, non_blocking=True)
            
            # zero the parameter gradients
            optimizer.zero_grad()
            
            # forward + backward + optimize
            output = model(data)        
            loss = criterion(output, target)
            preds = torch.max(output, 1)[1]

            loss.backward()
            optimizer.step()

            # There are 4 GPUs on one machine
            # N (the number of nodes): 1, G (the number of GPUs per node): 4
            # each process to span one GPU, so we have 4 processes
            # world_size: 4   (The total number of application processes running across all the nodes at one time is called the World Size)
            # total training data: 50000
            # batch_size: 100, iteration: 500, 
            # 500 / 4 = 125 iteration per GPU
            # 50000 / 4 = 12500 images per GPU
            if RANK in {-1, 0}:
                tloss += loss.item()
                accuracy += torch.eq(preds, target).sum().item()
                total_num += target.size(0)

        scheduler.step()
        if RANK in {-1, 0}:
            train_loss = tloss / (i + 1)
            train_acc = accuracy / total_num
            print(f'Train_local_rank: {LOCAL_RANK} Train_Epoch: {epoch}/{opt.epochs-1} Training_Loss: {train_loss} Training_acc: {train_acc}')




        if RANK in {-1, 0}:
            # valid
            model.eval()
            tloss = 0.0
            accuracy = 0.0
            total_num = 0
            with torch.no_grad():
                for _, (data, target) in enumerate(valid_loader):
                    data = data.to(device, non_blocking=True)
                    target = target.to(device, non_blocking=True)

                    outputs = model(data)
                    preds = torch.max(outputs, 1)[1]

                    loss = criterion(outputs, target)

                    tloss += loss.item() 
                    accuracy += torch.eq(preds, target).sum().item()
                    total_num += target.size(0)

            valid_loss = tloss / (i + 1)
            valid_acc = accuracy / total_num
            print(f'Valid_local_rank: {LOCAL_RANK} Valid_Epoch: {epoch}/{opt.epochs-1} Valid_Loss: {valid_loss} Valid_acc: {valid_acc}')
        if RANK in {-1, 0}:
            print(f'\n{epoch - start_epoch + 1} epochs completed in {(time.time() - t0) / 3600:.3f} hours.')

if __name__ == '__main__':
    opt = parse_opt()
    main(opt)    