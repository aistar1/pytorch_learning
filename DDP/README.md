# Definitions
`Node` - A physical instance or a container; maps to the unit that the job manager works with.  
`Worker` - A worker in the context of distributed training.  
`RANK` - The rank of the worker within a worker group.  
`WORLD_SIZE` - The total number of application processes running across all the nodes at one time.  
`LOCAL_RANK` - The rank of the worker within a local worker group.  
`LOCAL_WORLD_SIZE` - The number of processes running on each node.  


To illustrate the terminology defined above, consider the case where a DDP application is launched on two nodes, each of which has four GPUs. We would then like each process to span two GPUs each. The mapping of processes to nodes is shown in the figure below:

![image](https://user-images.githubusercontent.com/875518/77676984-4c81e400-6f4c-11ea-87d8-f2ff505a99da.png)


</br>
</br>
example 2

```
假設使用兩台機器，每台機器有3個GPU，每個GPU使用一個進程
機器1的 node_rank 序號為 0； 機器2的 node_rank 序號為 1
機器1的 rank 序號為 0, 1, 2； 機器2的 rank 序號為 3, 4, 5
機器1的 local_rank 序號為 0, 1, 2；機器2的 local_rank 序號為 0, 1, 2

Node=2
WORLD_SIZE = 2 * 3 = 6

```


# Running

#### Using docker
```
docker run -it --gpus all --rm -v $(pwd):/mnt --network=host --shm-size='6gb' pytorch/pytorch:2.0.0-cuda11.7-cudnn8-runtime
```


#### The parameter of torchrun 
[here](https://pytorch.org/docs/stable/distributed.html#launch-utility)

`nnodes` - number of nodes participating in distributed configuration.  
`node_rank` - Current machine index.  
`nproc_per_node` - number of processes per node to specify. (a good rule of thumb is to have one process span a single GPU)  

#### Use specific GPUs

```
CUDA_VISIBLE_DEVICES=0,1,2,3 torchrun --nproc_per_node=4 train_ddp.py
```



#### Use Multiple machines

```
# On master machine 0
torchrun --nproc_per_node=4 --nnodes=2 --node_rank=0 --master_addr="192.168.1.1" --master_port=1234 train_ddp.py

# torchrun --nproc_per_node G --nnodes N --node_rank 0 --master_addr "192.168.1.1" --master_port 1234 train.py
```

```
# On machine R
torchrun --nproc_per_node=4 --nnodes=2 --node_rank=1 --master_addr="192.168.1.1" --master_port=1234 train_ddp.py

# torchrun --nproc_per_node G --nnodes N --node_rank R --master_addr "192.168.1.1" --master_port 1234 train.py
```

where G is number of GPU per machine, N is the number of machines, and R is the machine number from 0...(N-1). Let's say I have two machines with two GPUs each, it would be G = 2 , N = 2, and R = 1 for the above.

Training will not start until all N machines are connected. Output will only be shown on master machine!



#### kill the process
```
kill $(ps aux | grep train_ddp.py | grep -v grep | awk '{print $2}')
```


#### Using ping to check connection
```
apt-get update
apt-get install dialog
apt-get install iputils-ping -y
```

```
ping 192.168.1.1
```

using ifconfig to check the IP address
```
ifconfig
```

NCCL would be instructed to use that IP address as you've set NCCL_SOCKET_IFNAME=eno1

```
export NCCL_SOCKET_IFNAME=eno1
```

# References
https://bobondemon.github.io/2020/12/20/Distributed-Data-Parallel-and-Its-Pytorch-Example/

https://leimao.github.io/blog/PyTorch-Distributed-Training/

https://blog.csdn.net/weixin_41041772/article/details/109820870

https://pytorch.org/docs/stable/elastic/run.html#launcher-api

https://github.com/pytorch/examples/tree/main/distributed/ddp
