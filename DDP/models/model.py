import torch
import torch.nn as nn
import torchvision.models as models
from torchvision.models._utils import IntermediateLayerGetter

class resnet18(nn.Module):
    def __init__(self, nc=1000):
        super().__init__()
        backbone = models.resnet18(pretrained=True)
        return_layers = {'avgpool': 'feat'}
        self.body = IntermediateLayerGetter(backbone, return_layers=return_layers)
        print(f'return_layers: {[name for name, _ in backbone.named_children()]}')
        self.fc1 = nn.Linear(512, nc)

    def forward(self, input):
        output = self.body(input)
        output = list(output.values())
        output = torch.flatten(output[0], 1) # flatten all dimensions except batch
        output = self.fc1(output)
        return output